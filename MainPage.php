<?php
require "ProductVO.php";
require "application/config/config.php";

use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Search;
use ApaiIO\ApaiIO;

if (isset($_POST['searchValue']) && $_POST['searchValue'] != '') {
    $searchValue = $_POST['searchValue'];

    $conf = getApaIOConf();

    $response = performKeywordSearch($conf, $searchValue);

    $amazonProducts = mapAmazonProducts($response);

    echo json_encode($amazonProducts);
}

function getApaIOConf()
{
    $conf = new GenericConfiguration();
    $conf->setResponseTransformer(new \ApaiIO\ResponseTransformer\XmlToSimpleXmlObject());
    $client = new \GuzzleHttp\Client();
    $request = new \ApaiIO\Request\GuzzleRequest($client);

    $conf
        ->setCountry('com')
        ->setAccessKey(AWS_ACCESS_KEY)
        ->setSecretKey(AWS_SECRET_KEY)
        ->setAssociateTag(AWS_ASSOCIATE_TAG)
        ->setRequest($request);

    return $conf;
}

function performKeywordSearch($conf, $keyword)
{
    $search = new Search();
    $search->setCategory('All');
    $search->setResponsegroup(array('Large'));
    $search->setPage(1);
    $search->setKeywords($keyword);

    $apaiIO = new ApaiIO($conf);

    $response = $apaiIO->runOperation($search);

    return $response;
}

function mapAmazonProducts($response)
{
    $products = [];
    $productLength = count($response->Items->Item);

    for ($i = 0; $i < $productLength; $i++) {
        $product = new Product();

        $product->setBrand((string)$response->Items->Item[$i]->ItemAttributes->Brand);
        $product->setTitle((string)$response->Items->Item[$i]->ItemAttributes->Title);
        $product->setPhoto((string)$response->Items->Item[$i]->MediumImage->URL);

        $products[] = $product;
    }

    return $products;
}

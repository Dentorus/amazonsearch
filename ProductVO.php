<?php

class Product
{
    public $title, $brand, $photo;

    function setBrand($brand)
    {
        $this->brand = $brand;
    }

    function setTitle($title)
    {
        $this->title = $title;
    }

    function setPhoto($photo)
    {
        $this->photo = $photo;
    }
}
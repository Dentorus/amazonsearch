$(document).ready(function () {
    $("#submit").click(function () {
        var inputText = $("#searchArea").val();

        console.log(inputText);
        $.ajax({
            url: 'MainPage.php',
            type: 'POST',
            data: ({searchValue: inputText}),
            dataType: "json",

            success: function (response) {

                console.log(response);
                if (response != '') {

                    var template = $("#usageList").html();
                    $("#target").html(_.template(template)({items: response}));

                    //$('#message').html(testResult);

                } else {
                    $('#message').html('<font color="red">Ничего не найдено по вашему запросу!!!</font>');
                }
            },
            error: function (response) {

                console.log(response);

                $('#message').html('<font color="red">ERROR</font>');
            }
        });
    });
});
